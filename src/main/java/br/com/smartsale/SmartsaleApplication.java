package br.com.smartsale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartsaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartsaleApplication.class, args);
	}

}
