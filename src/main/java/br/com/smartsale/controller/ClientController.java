package br.com.smartsale.controller;

import br.com.smartsale.controller.dto.request.ClientRequest;
import br.com.smartsale.controller.dto.response.ClientResponse;
import br.com.smartsale.model.Client;
import br.com.smartsale.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/save")
    public void save(@RequestBody ClientRequest clientRequest) {
        this.clientService.save(clientRequest);
    }

    @GetMapping("/all")
    public Page<ClientResponse> findAll(Pageable pageable) {

        return this.clientService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Client findById(@PathVariable Long id) {
        return this.clientService.findById(id);
    }

    @GetMapping("/name")
    public List<Client> findByName(@RequestParam String name) {
        return this.clientService.findByName(name);
    }

    @GetMapping("/cpf")
    public Client findByCpf(@RequestParam String cpf) {
        return this.clientService.findByCpf(cpf);
    }

    @PutMapping("/{id}")
    public Client update(@PathVariable Long id, @RequestBody ClientRequest clientRequest) {
        return this.clientService.update(id, clientRequest);
    }

    @PatchMapping("/status/{id}")
    public Client changeStatus(@PathVariable Long id) {
        return this.clientService.changeStatus(id);
    }


}
