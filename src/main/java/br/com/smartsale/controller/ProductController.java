package br.com.smartsale.controller;


import br.com.smartsale.controller.dto.request.ProductRequest;
import br.com.smartsale.controller.dto.response.ProductResponse;
import br.com.smartsale.model.Product;
import br.com.smartsale.model.Sale;
import br.com.smartsale.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @PostMapping("/save")
    public void save(@RequestBody ProductRequest productRequest) {
        this.productService.save(productRequest);
    }

    @GetMapping("/all")
    public Page<ProductResponse> findAll(Pageable pageable) {
        return this.productService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable Long id) {
        return productService.findById(id);
    }

    @GetMapping("/name")
    public List<Product> findByName(@RequestParam String name) {
        return productService.findByName(name);
    }

    @GetMapping("/barcode")
    public List<Product> findByBarcode(@RequestParam String barcode) {
        return productService.findByBarcode(barcode);
    }

    @PutMapping("/{id}")
    public Product update(@PathVariable Long id, @RequestBody ProductRequest productRequest) {
        return productService.update(id, productRequest);
    }

    @PatchMapping("/status/{id}")
    public Product changeStatus(@PathVariable Long id) {
        return productService.changeStatus(id);
    }


}
