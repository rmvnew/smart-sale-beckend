package br.com.smartsale.controller;

import br.com.smartsale.controller.dto.request.SaleRequest;
import br.com.smartsale.controller.dto.response.SaleResponse;
import br.com.smartsale.model.Sale;
import br.com.smartsale.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/sale")
public class SaleController {

    @Autowired
    private SaleService saleService;

    @PostMapping("/save")
    public void save(@RequestBody SaleRequest saleRequest) {
        this.saleService.save(saleRequest);
    }

    @GetMapping("/all")
    private Page<SaleResponse> findAll(Pageable pageable) {
        return this.saleService.findAll(pageable);
    }

}
