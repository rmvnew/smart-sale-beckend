package br.com.smartsale.controller.dto.mapper;

import br.com.smartsale.controller.dto.response.ProductResponse;
import br.com.smartsale.model.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {

//     ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

     ProductResponse toProductResponse(Product product);


}
