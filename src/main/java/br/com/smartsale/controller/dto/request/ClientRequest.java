package br.com.smartsale.controller.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientRequest {

    private String clientName;

    private String clientCpf;

}
