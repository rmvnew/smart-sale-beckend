package br.com.smartsale.controller.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {

    private String productName;

    private String productDescription;

    private String barcode;

    private Integer quantity;

    private Double price;


}
