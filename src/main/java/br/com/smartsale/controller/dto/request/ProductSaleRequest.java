package br.com.smartsale.controller.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSaleRequest {

    private Long productId;

    private Integer quantity;

    private Double price;

}
