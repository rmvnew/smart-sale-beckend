package br.com.smartsale.controller.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SaleRequest {

    private String clientCpf;

    private List<ProductSaleRequest> products;


}
