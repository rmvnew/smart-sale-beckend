package br.com.smartsale.controller.dto.response;

import br.com.smartsale.model.Client;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientResponse {

    private Long clientId;

    private String clientName;

    private String clientCpf;

    public ClientResponse(Client client) {
        this.clientId = client.getClientId();
        this.clientName = client.getClientName();
        this.clientCpf = client.getClientCpf();
    }

    public static ClientResponse converter(Client client){

        return new ClientResponse(client);

    }
}
