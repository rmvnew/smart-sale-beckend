package br.com.smartsale.controller.dto.response;

import br.com.smartsale.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ProductResponse {

    private Long productId;

    private String productName;

    private String barcode;

    private Double price;

    public ProductResponse(Product product) {
        this.productId = product.getProductId();
        this.productName = product.getProductName();
        this.barcode = product.getBarcode();
        this.price = product.getPrice();
    }

    public static ProductResponse converter(Product product){
        return new ProductResponse(product);
    }

}
