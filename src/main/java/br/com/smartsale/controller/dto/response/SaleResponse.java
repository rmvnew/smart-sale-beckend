package br.com.smartsale.controller.dto.response;

import br.com.smartsale.model.Sale;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class SaleResponse {

    private Long saleId;

    private ClientResponse clientResponse;

    private List<ProductResponse> productsResponses;

    private BigDecimal total;

    public static SaleResponse converter(Sale sale){

        var client = new ClientResponse(sale.getClient());

        List<ProductResponse> products = new ArrayList<>();
        sale.getProducts().forEach(data ->{
            products.add(new ProductResponse(data));
        });

        var currentSale = new SaleResponse();
        currentSale.setSaleId(sale.getSaleId());
        currentSale.setClientResponse(client);
        currentSale.setProductsResponses(products);
        currentSale.setTotal(sale.getTotal());

        return currentSale;
    }

}
