package br.com.smartsale.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ProductAlreadyRegisteredException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ProductAlreadyRegisteredException(String message) {
        super(message);
    }
}
