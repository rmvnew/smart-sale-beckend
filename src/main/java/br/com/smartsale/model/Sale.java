package br.com.smartsale.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SALE")
public class Sale {
    @Id
    @Column(name = "sale_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long saleId;

    @ManyToOne
    @JoinColumn(name = "client_id",nullable = false)
    private Client client;

    @ManyToMany
    @JoinTable(
            name = "PRODUCT_SALE",
            uniqueConstraints = @UniqueConstraint(columnNames = {"sale_id","product_id"}),
            joinColumns = @JoinColumn(name = "sale_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products;

    @Column
    private BigDecimal total;

    @Column(name = "create_at")
    private LocalDateTime createAt;



}
