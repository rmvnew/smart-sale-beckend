package br.com.smartsale.repository;

import br.com.smartsale.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "SELECT * FROM CLIENT WHERE client_name like %:name% AND is_active = true",nativeQuery = true)
    Optional<List<Client>> findByName(@Param("name") String name);

    @Query(value = "SELECT is_active FROM CLIENT WHERE client_id = :id AND is_active = false",nativeQuery = true)
    Optional<Boolean> findInactive(@Param("id") Long id);

    @Query(value = "SELECT * FROM CLIENT WHERE client_cpf like %:cpf% AND is_active = true",nativeQuery = true)
    Optional<Client> findByCpf(@Param("cpf") String cpf);

//    @Query(value = "SELECT * FROM CLIENT WHERE client_cpf like %:cpf% AND is_active = true ",nativeQuery = true)
//    Optional<List<Client>> findByCpfs(@Param("cpf") String clientCpf);
}
