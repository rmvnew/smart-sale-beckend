package br.com.smartsale.repository;

import br.com.smartsale.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


    @Query(value = "SELECT * FROM PRODUCT WHERE product_name = :productName AND is_active = true",nativeQuery = true)
    Optional<Product> findName(@Param("productName") String productName);

    @Query(value = "SELECT * FROM PRODUCT WHERE barcode = :barcode AND is_active = true",nativeQuery = true)
    Optional<Product> findBarcode(@Param("barcode") String barcode);

    @Query(value = "SELECT * FROM PRODUCT WHERE product_name like %:name% AND is_active = true",nativeQuery = true)
    Optional<List<Product>> findByName(@Param("name") String name);

    @Query(value = "SELECT is_active FROM PRODUCT WHERE product_id = :id AND is_active = false",nativeQuery = true)
    Optional<Boolean> findInactive(@Param("id") Long id);

    @Query(value = "SELECT * FROM PRODUCT WHERE barcode like %:barcode% AND is_active = true",nativeQuery = true)
    Optional<List<Product>> findByBarcode(@Param("barcode") String barcode);




}
