package br.com.smartsale.service;

import br.com.smartsale.controller.dto.request.ClientRequest;
import br.com.smartsale.controller.dto.response.ClientResponse;
import br.com.smartsale.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClientService {

    void save(ClientRequest clientRequest);

    Page<ClientResponse> findAll(Pageable pageable);

    Client findById(Long id);

    Client findByCpf(String cpf);

    List<Client> findByName(String name);

    Client update(Long id, ClientRequest clientRequest);

    Client changeStatus(Long id);

}
