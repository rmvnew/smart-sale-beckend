package br.com.smartsale.service;


import br.com.smartsale.controller.dto.request.ProductRequest;
import br.com.smartsale.controller.dto.response.ProductResponse;
import br.com.smartsale.model.Product;
import br.com.smartsale.model.Sale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {

    void save(ProductRequest productRequest);

    Page<ProductResponse> findAll(Pageable pageable);

    Product findById(Long id);

    List<Product> findByName(String name);

    List<Product> findByBarcode(String barcode);

    Product update(Long id, ProductRequest productRequest);

    Product changeStatus(Long id);

    void updateQuantity(Long productId, Integer quantity);
}
