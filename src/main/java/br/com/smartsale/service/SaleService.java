package br.com.smartsale.service;

import br.com.smartsale.controller.dto.request.SaleRequest;
import br.com.smartsale.controller.dto.response.SaleResponse;
import br.com.smartsale.model.Sale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SaleService {

    void save(SaleRequest saleRequest);

    Page<SaleResponse> findAll(Pageable pageable);

    Sale findById(Long id);

    Sale findByCpf(Long id);


}
