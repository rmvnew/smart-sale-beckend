package br.com.smartsale.service.impl;

import br.com.smartsale.controller.dto.request.ClientRequest;
import br.com.smartsale.controller.dto.response.ClientResponse;
import br.com.smartsale.exception.ClientAlreadyRegisteredException;
import br.com.smartsale.exception.ClientNotFoundException;
import br.com.smartsale.model.Client;
import br.com.smartsale.repository.ClientRepository;
import br.com.smartsale.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Override
    public void save(ClientRequest clientRequest) {

        boolean nameIsRegistered = this.findByName(clientRequest.getClientName().toUpperCase(Locale.ROOT)).isEmpty();

        if(!nameIsRegistered){
            throw  new ClientAlreadyRegisteredException(
                    String.format("1- Client %s is already registered",clientRequest.getClientName().toUpperCase(Locale.ROOT))
            );
        }

        boolean cpfIsRegistered = this.clientRepository.findByCpf(clientRequest.getClientCpf()).isPresent();

        if(cpfIsRegistered){
            throw  new ClientAlreadyRegisteredException(
                    String.format("2- Client %s is already registered",clientRequest.getClientCpf())
            );
        }

        Client client = new Client();
        client.setClientName(clientRequest.getClientName().toUpperCase(Locale.ROOT));
        client.setClientCpf(clientRequest.getClientCpf());
        client.setIsActive(true);
        client.setCreateAt(LocalDateTime.now(ZoneOffset.UTC));
        client.setUpdateAt(LocalDateTime.now(ZoneOffset.UTC));

        this.clientRepository.save(client);

    }

    @Override
    public Page<ClientResponse> findAll(Pageable pageable) {
        return new PageImpl<>(this.clientRepository.findAll(pageable)
                .stream()
                .map(ClientResponse::converter)
                .collect(Collectors.toList()));
    }

    @Override
    public Client findById(Long id) {
        return this.clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(
                        String.format("The client with id %d was not found", id)
                ));
    }

    @Override
    public Client findByCpf(String cpf) {
        return this.clientRepository.findByCpf(cpf)
                .orElseThrow(() -> new ClientNotFoundException(
                        String.format("The client with cpf %d was not found", cpf)
                ));
    }

    @Override
    public List<Client> findByName(String name) {
        return this.clientRepository.findByName(name.toUpperCase(Locale.ROOT))
                .orElseThrow(() -> new ClientNotFoundException(
                        String.format("The client with name %s was not found", name)
                ));
    }


    @Override
    public Client update(Long id, ClientRequest clientRequest) {

        boolean isActive = this.clientRepository.findInactive(id).isEmpty();

        if (!isActive) {
            throw new ClientNotFoundException(
                    String.format("The client with id %d was not found", id)
            );
        }

        Client clientSaved = this.clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(
                        String.format("The client with id %d was not found", id)
                ));


        clientSaved.setClientName(clientRequest.getClientName().toUpperCase(Locale.ROOT));
        clientSaved.setClientCpf(clientRequest.getClientCpf());
        clientSaved.setUpdateAt(LocalDateTime.now(ZoneOffset.UTC));

        this.clientRepository.save(clientSaved);

        return this.findById(id);
    }

    @Override
    public Client changeStatus(Long id) {

        Client clientSaved = this.clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(
                        String.format("The client with id %d was not found", id)
                ));

        boolean status = clientSaved.getIsActive();

        clientSaved.setIsActive(!status);

        this.clientRepository.save(clientSaved);

        return this.findById(id);
    }
}
