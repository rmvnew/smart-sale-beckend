package br.com.smartsale.service.impl;

import br.com.smartsale.controller.dto.request.ProductRequest;
import br.com.smartsale.controller.dto.response.ProductResponse;
import br.com.smartsale.exception.ProductAlreadyRegisteredException;
import br.com.smartsale.exception.ProductNotFoundException;
import br.com.smartsale.model.Product;
import br.com.smartsale.model.Sale;
import br.com.smartsale.repository.ProductRepository;
import br.com.smartsale.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void save(ProductRequest productRequest) {

        String currentName = productRequest.getProductName().toUpperCase(Locale.ROOT);
        String currentDescription = productRequest.getProductDescription().toUpperCase(Locale.ROOT);

        var registeredName = this.productRepository
                .findName(productRequest.getProductName().toUpperCase(Locale.ROOT)).isPresent();

        if (registeredName) {
            throw new ProductAlreadyRegisteredException(
                    String.format("1- Product %s is already registered", productRequest.getProductName())
            );
        }

        var registeredBarcode = this.productRepository
                .findBarcode(productRequest.getBarcode()).isPresent();

        if (registeredBarcode) {
            throw new ProductAlreadyRegisteredException(
                    String.format("2- Product %s is already registered", productRequest.getProductName())
            );
        }

        Product product = new Product();
        product.setProductName(currentName);
        product.setProductDescription(currentDescription);
        product.setBarcode(productRequest.getBarcode());
        product.setQuantity(productRequest.getQuantity());
        product.setPrice(productRequest.getPrice());
        product.setIsActive(true);
        product.setCreateAt(LocalDateTime.now(ZoneOffset.UTC));
        product.setUpdateAt(LocalDateTime.now(ZoneOffset.UTC));

        this.productRepository.save(product);


    }


    @Override
    public Page<ProductResponse> findAll(Pageable pageable) {
        return new PageImpl<>(this.productRepository.findAll(pageable)
                .stream()
                .map(ProductResponse::converter)
                .collect(Collectors.toList()));
    }


    @Override
    public Product findById(Long id) {
        return this.productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(
                        String.format("the product with id: %d was not found", id)
                ));
    }

    @Override
    public List<Product> findByName(String name) {
        return this.productRepository.findByName(name)
                .orElseThrow(() -> new ProductNotFoundException(
                        String.format("the product with name: %s was not found", name)
                ));
    }

    @Override
    public List<Product> findByBarcode(String barcode) {
        return this.productRepository.findByBarcode(barcode)
                .orElseThrow(() -> new ProductNotFoundException(
                        String.format("the product with barcode: %s was not found", barcode)
                ));
    }


    @Override
    public Product update(Long id, ProductRequest productRequest) {

        boolean isActive = this.productRepository.findInactive(id).isEmpty();

        if (!isActive) {
            throw new ProductNotFoundException(
                    String.format("the product with id: %d was not found", id)
            );
        }

        Product productSaved = this.productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(
                        String.format("the product with id: %d was not found", id)
                ));


        productSaved.setProductName(productRequest.getProductName().toUpperCase(Locale.ROOT));
        productSaved.setProductDescription(productRequest.getProductDescription().toUpperCase(Locale.ROOT));
        productSaved.setBarcode(productRequest.getBarcode());
        productSaved.setQuantity(productRequest.getQuantity());
        productSaved.setPrice(productRequest.getPrice());
        productSaved.setUpdateAt(LocalDateTime.now(ZoneOffset.UTC));

        this.productRepository.save(productSaved);

        return this.findById(id);
    }

    @Override
    public Product changeStatus(Long id) {

        Product productSaved = this.productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(
                        String.format("the product with id: %d was not found", id)
                ));

        boolean status = productSaved.getIsActive();

        productSaved.setIsActive(!status);

        this.productRepository.save(productSaved);

        return this.findById(id);

    }

    @Override
    public void updateQuantity(Long id, Integer quantity) {
        Product prod = this.findById(id);
        int newQuantity = prod.getQuantity() - quantity;
        prod.setQuantity(newQuantity);
        this.productRepository.save(prod);
    }
}
