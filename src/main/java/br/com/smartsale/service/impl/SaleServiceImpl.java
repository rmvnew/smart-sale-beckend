package br.com.smartsale.service.impl;

import br.com.smartsale.controller.dto.request.ProductSaleRequest;
import br.com.smartsale.controller.dto.request.SaleRequest;
import br.com.smartsale.controller.dto.response.SaleResponse;
import br.com.smartsale.model.Client;
import br.com.smartsale.model.Product;
import br.com.smartsale.model.Sale;
import br.com.smartsale.repository.SaleRepository;
import br.com.smartsale.service.ClientService;
import br.com.smartsale.service.ProductService;
import br.com.smartsale.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProductService productService;

    @Override
    public void save(SaleRequest saleRequest) {

        Client client = this.clientService.findByCpf(saleRequest.getClientCpf());
        List<ProductSaleRequest> productsRequest = saleRequest.getProducts();
        double total = productsRequest.stream()
                .mapToDouble(prod -> (prod.getPrice() * prod.getQuantity()))
                .reduce(0, Double::sum);

        List<Product> products = new ArrayList<>();
        productsRequest.forEach(data -> {

            products.add(this.productService.findById(data.getProductId()));
            this.productService.updateQuantity(data.getProductId(), data.getQuantity());

        });


        Sale sale = new Sale();
        sale.setClient(client);
        sale.setTotal(new BigDecimal(total));
        sale.setProducts(products);
        sale.setCreateAt(LocalDateTime.now(ZoneOffset.UTC));


        this.saleRepository.save(sale);

    }

    @Override
    public Page<SaleResponse> findAll(Pageable pageable) {

        return new PageImpl<>(this.saleRepository.findAll(pageable)
                .stream()
                .map(SaleResponse::converter).collect(Collectors.toList()));
    }

    @Override
    public Sale findById(Long id) {
        return null;
    }

    @Override
    public Sale findByCpf(Long id) {
        return null;
    }
}
